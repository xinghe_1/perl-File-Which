Name:           perl-File-Which
Version:        1.23
Release:        1
Summary:        Perl implementation of the which utility as an API
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/File-Which
Source0:        https://cpan.metacpan.org/authors/id/P/PL/PLICEASE/File-Which-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) perl(Env)
BuildRequires:  perl(strict) perl(constant) perl(Exporter) perl(warnings) perl(Config)
BuildRequires:  perl(File::Spec) >= 0.60
BuildRequires:  perl(Test::More) >= 0.80
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))
Requires:       perl(File::Spec) >= 0.60

%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(File::Spec\\)$

%description
File::Which finds the full or relative paths to executable programs on the system. This is
normally the function of which utility. which is typically implemented as either a program
or a built in shell command. On some platforms, such as Microsoft Windows it is not provided
as part of the core operating system. This module provides a consistent API to this
functionality regardless of the underlying platform.

The focus of this module is correctness and portability. As a consequence platforms where the
current directory is implicitly part of the search path such as Microsoft Windows will find
executables in the current directory, whereas on platforms such as UNIX where this is not the
case executables in the current directory will only be found if the current directory is
explicitly added to the path.

If you need a portable which on the command line in an environment that does not provide it,
install App::pwhich which provides a command line interface to this API.

%package_help

%prep
%autosetup -n File-Which-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
chmod -R u+w $RPM_BUILD_ROOT

%check
make test

%files
%doc Changes README
%license LICENSE
%{perl_vendorlib}/File/

%files help
%{_mandir}/man3/*

%changelog
* Wed Jul 23 2020 xinghe <xinghe1@huawei.com> - 1.23-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.23

* Fri Sep 27 2019 shenyangyang<shenyangyang4@huawei.com> - 1.22-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add the license file

* Sun Sep 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.22-5
- Package init
